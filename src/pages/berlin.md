---
layout: page
title: "Berlin"
permalink: "/berlin/"
---

<img src="/assets/img/berlin.jpg">

<ul>
  {% for place in site.berlin %}
    <li>
      {{ place.type }} <a href="{{ place.url }}">{{ place.title }}</a> - {{ place.headline }}
      [{% for tag in place.tags %}
        {{ tag }}
      {% endfor %}]
    </li>
  {% endfor %}
</ul>
