---
layout: page
title: "San Francisco"
permalink: "/sanfrancisco/"
---

<img src="/assets/img/sanfrancisco.jpg">

<ul>
  {% for place in site.sanfrancisco %}
    <li>
      {{ place.type }} <a href="{{ place.url }}">{{ place.title }}</a> - {{ place.headline }}
      [{% for tag in place.tags %}
        {{ tag }}
      {% endfor %}]
    </li>
  {% endfor %}
</ul>
