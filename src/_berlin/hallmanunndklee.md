---
layout: page
title: "Hallmann und Klee"
headline: "Breakfast Lunch & Dinner"
type: 🍳
tags:
  breakfast
  bread
  pastry
---

Good any time of the day but especially for breakfast.

Böhmische Str. 13

https://www.hallmann-klee.de/

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2430.29267747425!2d13.447550916186055!3d52.473836279805525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a84f0b246da06b%3A0x5c26a8b442557d!2sHallmann+und+Klee!5e0!3m2!1sen!2sde!4v1542059605400" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>