---
layout: page
title: "Warakubune"
headline: "Sushi Restaurant"
type: 🍣
tags:
  sushi
---

Sushi boat place. Always good.

307 Church St

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3154.027009245863!2d-122.43081038418919!3d37.765964879761064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808f7e1e96c60919%3A0x79e7aaf1d2897301!2sWarakubune!5e0!3m2!1sen!2sde!4v1542059781300" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>